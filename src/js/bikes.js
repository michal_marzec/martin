/**
 * unused variable 'markers'
 */
const bikes = [],
    stationNames = [],
    markers = [];

/**
 * unused variable
 */
let allShown = false;

function addBikeStations() {
    bikes.map(station => {
        stationNames.push(station.properties.label);
    });
    addStationNames();
}

function addStationNames() {
    const sorted = stationNames.sort((a, b) => {
        if (a > b) return 1;
        return -1;
    });
    sorted.map(item => {
        bikeItems.innerHTML += `<li class="dropdown-item bike-item">${item}</li>`;
    });
}

function showSelectedStation(event) {
    const selected = event.target.innerHTML;
    /**
     * Comparison of numbers is more efficient;
     * Maybe you should try to use some kind of index here;
     */
    if (selected === "Wszystkie") {
        bikes.map(station => {
            stationNames.push(station.properties.label);
            let coordinates = station.geometry.coordinates.reverse();
            /**
             * var used instead of let or const
             */
            var marker = L.marker([...coordinates]).addTo(mymap);
            marker.bindPopup(
                `${station.properties.label}<br>
              Numer stacji: ${station.id}<br>
              Dostępne rowery: ${station.properties.bikes}<br>
              Razem stojaków: ${station.properties.bike_racks}<br>
              Wolnych stojaków: ${station.properties.free_racks}<br>
              Ostatnia aktualizacja:<br>
              ${station.properties.updated}
              `
            );
        });
    }
}
