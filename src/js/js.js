// District maps handling

const districts = [], // data from dataloader.js
  polygons = [],
  allCoordinates = [],
  wholeMapBorders = [];

function addDistrictsMap() {
  districts.map(e => {
    const coordinates = e.geometry.coordinates;
    allCoordinates.push(coordinates);
    const name = e.properties.name;
    showDistrictsMap(coordinates, name);
  });
  getAllDistrictsBounds();
}

function showDistrictsMap(longLat, name) {
  /**
   * contents of collections lat and long are updated, but never queried
   */
  const lat = [];
  const long = [];
  longLat[longLat.length - 1].map(e => {
    e.reverse();
    lat.push(e[0]);
    long.push(e[1]);
  });
  const polygon = L.polygon(longLat, { color: "#1E90FF" }).addTo(mymap);
  polygon.bindPopup(name);
  polygons.push(polygon);
}

function addDistrictsNames() {
  districts
    .sort((a, b) => {
      if (a.properties.name > b.properties.name) return 1;
      return -1;
    })
    .map(district => {
      districtsSelect.innerHTML += `<li class="dropdown-item district-item">${
        district.properties.name
      }</li>`;
    });
}

function getSelectedDistrict(event) {
  const selected = event.target.innerHTML;

  if (selected === "Wszystkie") {
    closeAllPopups();
    fitAllDistrictsBounds();
  } else {
    const selectedDistrict = districts.filter(
      district => district.properties.name === selected
    );
    const name = selectedDistrict[0].properties.name;
    showSelectedDistrict(name);
  }
}

function showSelectedDistrict(name) {
  let latLong;

  polygons.map(p => {
    if (p._popup._content === name) {
      latLong = p._latlngs;
      p.openPopup();
    }
  });

  /**
   * Variable `latLong` might not have been initialised, add some kind of fallback or throw error;
   */
  mymap.fitBounds(latLong);
}

function closeAllPopups() {
  polygons.map(p => {
    p.closePopup();
  });
}

function getAllDistrictsBounds() {
  const long = [];
  const lat = [];
  allCoordinates.map(e => {
    e[0].map(e => {
      long.push(e[1]);
      lat.push(e[0]);
    });
  });
  const maxLong = Math.max(...long);
  const minLong = Math.min(...long);
  const maxLat = Math.max(...lat);
  const minLat = Math.min(...lat);
  wholeMapBorders.push([maxLat, maxLong], [minLat, minLong]);
}

function fitAllDistrictsBounds() {
  mymap.fitBounds(wholeMapBorders);
}
