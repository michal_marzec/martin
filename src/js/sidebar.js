const districtItems = document.querySelector("#districts-items"),
  districtsSelect = document.querySelector("#districts-items"),
  bikeItems = document.querySelector("#bikes-items");

districtsSelect.onclick = getSelectedDistrict;
bikeItems.onclick = showSelectedStation;

districtItems.onmouseenter = openDropdown;
districtItems.onmouseleave = closeDropdown;
bikeItems.onmouseenter = openDropdown;
bikeItems.onmouseleave = closeDropdown;

function openDropdown() {
  this.style.height = "300px";
  this.style.transition = "all 0.5s";
  this.classList.add("dropdown-open");
}

function closeDropdown() {
  this.style.height = "1.8rem";
  /**
   * Unused definition `scrollTop`;
   */
  this.scrollTop = 0;
  this.classList.remove("dropdown-open");
  this.style.transition = "all 0.5s";
}
