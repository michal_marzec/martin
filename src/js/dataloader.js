(function data() {
    fetch(
        "http://www.poznan.pl/mim/plan/map_service.html?mtype=local_gov&co=osiedla"
    )
        .then(res => res.json())
        .then(data =>
            /**
             * Map should return changed item in callback function,
             * now it pushes the same value into unrelated array.
             *
             * e.g.:
             * let myArray = [2, 3, 4, 5, 35];
             * myArray = myArray.map(item => item * 2);
             *
             * moreover this usage is pointless,
             * `data.features` should be passed directly to `addDistrictMap` function as an argument
             *
             * e.g.:
             * .then(data => data.features)
             * .then((features) => {
             *      addDistrictMap(features);
             *      addDistrictsNames(features);
             * })
             */
            data.features.map(district => {
                districts.push(district);
            })
        )
        .then(() => addDistrictsMap())
        .then(() => addDistrictsNames())
        .catch(err => {
            console.log(err);
        });
})();

(function bikesData() {
    fetch(
        "http://www.poznan.pl/mim/plan/map_service.html?mtype=pub_transport&co=stacje_rowerowe"
    )
        .then(res => res.json())
        .then(data =>
            data.features.map(station => {
                bikes.push(station);
            })
        )
        .then(() => addBikeStations())
        .catch(err => {
            console.log(err);
        });
})();
