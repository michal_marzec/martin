const
    gulp = require('gulp'),
    sass = require('gulp-sass'),
    connect = require('gulp-connect');

gulp.task('style', (done) => {
    gulp.src("./src/styles/scss/style.scss")
        .pipe(sass.sync())
        .pipe(gulp.dest("./dist/styles/css/"));
    done();
});

gulp.task('copyJS', (done) => {
    gulp.src('src/js/**/*.js')
        .pipe(gulp.dest('dist/js/'));
    done();
});

gulp.task('copyHTML', (done) => {
    gulp.src('src/index.html')
        .pipe(gulp.dest('dist/'));
    done();
});

gulp.task('connect', (done) => {
    connect.server({
        host: '0.0.0.0',
        root: "dist/",
        livereload: false,
        port: 6060,
        https: false
    });
    done();
});

gulp.task('watch', (done) => {
    gulp.watch("./src/styles/**/*.scss", gulp.series("style"));
    gulp.watch("./src/js/**/*.js", gulp.series("copyJS"));
    gulp.watch("./src/index.html", gulp.series("copyHTML"));
    done();
});

gulp.task('build', gulp.series('style', 'copyJS', 'copyHTML'));
gulp.task('default', gulp.series('build', 'connect', 'watch'));
